const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));

function style() {  // назва таска
    return gulp.src('src/styles/styles.scss')   // звідки беремо //{}
        .pipe(sass().on('error', sass.logError)) // що робимо з файлом
        .pipe(gulp.dest('./dist')) // куди складаємо результат
};


exports.style = style;